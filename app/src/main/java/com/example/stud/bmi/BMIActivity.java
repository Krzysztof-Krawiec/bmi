package com.example.stud.bmi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class BMIActivity extends AppCompatActivity {
    private static final String PREFERENCES_NAME = "myPreferences";
    private static final String SAVE_WEIGHT = "weight";
    private static final String SAVE_HEIGHT = "height";
    private static final String SAVE_DIMENSIONS = "savedDimensions";
    private EditText editTextWeight;
    private EditText editTextHeight;
    private TextView textViewWeightTV;
    private TextView textViewHeightTV;
    private Button buttonCount;
    protected boolean EnglishDimensions;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.main_menu, menu);
       menu.findItem(R.id.saveData).setVisible(true);
       this.invalidateOptionsMenu();
       return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.aboutMe) {
            Intent intentAboutMe = new Intent(BMIActivity.this, AboutActivity.class);
            startActivity(intentAboutMe);
            return true;
        }
        if(id == R.id.saveData){
            saveData();
            showToast(R.string.Saved);
            return true;
        }
        if(id == R.id.settings) {
            Intent intentSettings = new Intent(BMIActivity.this, SettingsActivity.class);
            startActivity(intentSettings);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveData() {
       try {
           SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFERENCES_NAME, 0);
           SharedPreferences.Editor editor = settings.edit();

           editor.putString(SAVE_WEIGHT, editTextWeight.getText().toString());
           editor.putString(SAVE_HEIGHT, editTextHeight.getText().toString());
           editor.putBoolean(SAVE_DIMENSIONS, EnglishDimensions);
           editor.apply();
       }
       catch (Exception e) {
       }
    }

    private void restoreData() {
        try {
            SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFERENCES_NAME, 0);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            EnglishDimensions = prefs.getBoolean("englishDim",false);
            changeTextBoxTitles();
            if(EnglishDimensions == settings.getBoolean(SAVE_DIMENSIONS, false)) {
                editTextWeight.setText(settings.getString(SAVE_WEIGHT, ""));
                editTextHeight.setText(settings.getString(SAVE_HEIGHT, ""));
            }
        }
        catch(Exception e) {
        }
    }

    private void initializeComponents()
    {
        buttonCount = findViewById(R.id.count);
        editTextWeight = findViewById(R.id.weight);
        editTextHeight = findViewById(R.id.height);
        textViewWeightTV = findViewById(R.id.weightTV);
        textViewHeightTV = findViewById(R.id.heightTV);
    }
    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean englishDimPref = prefs.getBoolean("englishDim",false);
        if ( englishDimPref != EnglishDimensions)
        {
            EnglishDimensions = englishDimPref;
            changeTextBoxTitles();
        }
    }
    private void changeTextBoxTitles() {
        if (EnglishDimensions) {
            textViewWeightTV.setText(R.string.weightEnglish);
            textViewHeightTV.setText(R.string.heightEnglish);
        }else {
            textViewWeightTV.setText(R.string.weightMetric);
            textViewHeightTV.setText(R.string.heightMetric);
        }
        editTextWeight.setText("");
        editTextHeight.setText("");
    }

    private void showToast(int id) {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, id, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void buttonCountAction(){
        double w, h, result;
        try {
            h = Double.parseDouble(editTextHeight.getText().toString());
            w = Double.parseDouble(editTextWeight.getText().toString());
            BMICalculator bmi;
            if (EnglishDimensions) {
                bmi = new EnglishDimensionsBMI( w, h );
            }
            else
                bmi = new MetricDimensionsBMI(w, h);

            Intent i = new Intent(BMIActivity.this, BmiCountedActivity.class);
            result =  bmi.count();
            i.putExtra("result", result);
            startActivity(i);
        }
        catch(IllegalArgumentException e) {
            showToast(R.string.error);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        initializeComponents();

        restoreData();

        buttonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buttonCountAction();
            }
        });
    }
}
