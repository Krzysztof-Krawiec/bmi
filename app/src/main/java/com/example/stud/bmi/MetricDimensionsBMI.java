package com.example.stud.bmi;

/**
 * Created by stud on 08.03.2018.
 */

public class MetricDimensionsBMI extends BMICalculator {
    private final static int validationMinWeight=30;
    private final static int validationMaxWeight=300;
    private final static int validationMinHeight=100;
    private final static int validationMaxHeight=260;
    MetricDimensionsBMI(double dWeight, double dHeight) {
        super(dWeight, dHeight);
    }

    public boolean validateData()
    {
        if(weight < validationMinWeight || weight > validationMaxWeight || height<validationMinHeight || height>validationMaxHeight)
            return false;
        else
            return  true;
    }
    public void setHeight(double dHeight)
    {
        height = dHeight;
    }

    public void setWeight(double dWeight)
    {
        weight = dWeight;
    }

    public double setHeight()
    {
        return height;
    }

    public double setWeight()
    {
        return weight;
    }

    @Override
    double count() {
        if (validateData())
        {
            double cmHeight=height / 100.0;
            return weight / (cmHeight*cmHeight);
        }
        else
        {
            throw new IllegalArgumentException("Błędne dane");
        }
    }
}
