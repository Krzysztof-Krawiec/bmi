package com.example.stud.bmi;

/**
 * Created by Krzysiek on 2018-03-09.
 */

public class EnglishDimensionsBMI extends BMICalculator {
    private final static int constantEnglishBMIFactor=703;
    private final static int validationMinWeight=70;
    private final static int validationMaxWeight=700;
    private final static int validationMinHeight=40;
    private final static int validationMaxHeight=100;

    EnglishDimensionsBMI(double dWeight, double dHeight) {
        super(dWeight, dHeight);
    }

    @Override
    void setHeight(double dHeight) {
        height = dHeight;
    }

    @Override
    void setWeight(double dWeight) {
        weight = dWeight;
    }

    @Override
    double setHeight() {
        return height;
    }

    @Override
    double setWeight() {
        return weight;
    }

    @Override
    double count() {
        if (validateData())
        {
            return (weight) / (height/100)*constantEnglishBMIFactor;
        }
        else
        {
            throw new IllegalArgumentException("Błędne dane");
        }
    }

    @Override
    boolean validateData() {
        if(weight>validationMaxWeight || weight<validationMinWeight || height>validationMaxHeight || height<validationMinHeight)
            return false;
        else
            return  true;
    }
}
