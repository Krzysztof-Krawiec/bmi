package com.example.stud.bmi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.DecimalFormat;

public class BmiCountedActivity extends AppCompatActivity {
    final static DecimalFormat df2 = new DecimalFormat(".##");
    private static final double underWeightBMI = 18.0;
    private static final double correctBMI = 24.0;
    private static final double overWeightBMI = 29;
    private static final double obesityBMI = 39;
    private double countedBMI;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.aboutMe) {
            Intent intentAboutMe = new Intent(BmiCountedActivity.this, AboutActivity.class);
            startActivity(intentAboutMe);
            return true;
        }
        if (id == R.id.settings) {
            Intent intentSettings = new Intent(BmiCountedActivity.this, SettingsActivity.class);
            startActivity(intentSettings);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void setColor(int color){
        getWindow().getDecorView().setBackgroundColor(getResources().getColor(color));
    }
    private String getResourceString(int string){
        return getResources().getString(string);
    }
    private void setBMIAppearance(double countedBMI){
        String descriptionBMI;
        if(countedBMI<=underWeightBMI) {
            setColor(R.color.blue);
            descriptionBMI=getResourceString(R.string.underWeightBMI);
        }else
        if(countedBMI<=correctBMI) {
            setColor(R.color.green);
            descriptionBMI = getResourceString(R.string.correctBMI);
        }else
        if(countedBMI<=overWeightBMI) {
            setColor(R.color.yellow);
            descriptionBMI =getResourceString(R.string.overWeightBMI);
        }else
        if(countedBMI<=obesityBMI) {
            setColor(R.color.orange);
            descriptionBMI = getResourceString(R.string.obesityBMI);
        }else {
            setColor(R.color.red);
            descriptionBMI = getResourceString(R.string.seriousObesityBMI);
        }

        if(countedBMI>0)
        {
            setText(R.id.countedBMI,df2.format(countedBMI));
            setText(R.id.descriptionBMI,descriptionBMI);
        }
    }

    private void setText(int TextViewId, String text){
        ((TextView) findViewById(TextViewId)).setText(text);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi_counted);
        countedBMI = getIntent().getDoubleExtra("result",-1);
        setBMIAppearance(countedBMI);
    }
}
